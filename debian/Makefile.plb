lib.name = chaos
class.sources = \
	attract1.c \
	base3.c \
	base.c \
	dejong.c \
	gingerbreadman.c \
	henon.c \
	hopalong.c \
	ikeda.c \
	latoocarfian.c \
	latoomutalpha.c \
	latoomutbeta.c \
	latoomutgamma.c \
	logistic.c \
	lorenz.c \
	lotkavolterra.c \
	martin.c \
	mlogistic.c \
	pickover.c \
	popcorn.c \
	quadruptwo.c \
	rossler.c \
	standardmap.c \
	strange1.c \
	tent.c \
	three_d.c \
	threeply.c \
	tinkerbell.c \
	unity.c \
	$(empty)
shared.sources = \
	libchaos.c \
	$(empty)
datafiles = \
	$(wildcard *.pd) \
	$(wildcard *.txt) \
	$(wildcard *.md) \
	$(empty)
datadirs = \
	examples \
	$(empty)

PDLIBBUILDER_DIR=/usr/share/pd-lib-builder
include $(PDLIBBUILDER_DIR)/Makefile.pdlibbuilder
